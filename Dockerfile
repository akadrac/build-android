FROM gradle:8-jdk17

ENV CMD_LINE_TOOLS=8512546
ENV ANDROID_SDK_ROOT=/opt/android
ENV PATH=$PATH:${ANDROID_SDK_ROOT}/cmdline-tools/latest/bin

RUN install -d $ANDROID_SDK_ROOT && \
  cd $ANDROID_SDK_ROOT && \
  wget -q https://dl.google.com/android/repository/commandlinetools-linux-${CMD_LINE_TOOLS}_latest.zip -O cmdline-tools.zip && \
  unzip -qq -d cmdline-tools cmdline-tools.zip && \
  rm cmdline-tools.zip && \
  mv cmdline-tools/cmdline-tools cmdline-tools/latest

RUN yes | sdkmanager --licenses
